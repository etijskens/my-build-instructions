#!/bin/bash

# 1. adjust boost_1_66_0/tools/build/example/user-config.jam and put it in your home directory

cd boost_1_66_0
./bootstrap.sh --with-python=/Users/etijskens/miniconda3/envs/pp2017/bin/python --with-python-version=3.6 --with-python-root=/Users/etijskens/miniconda3/envs/pp2017/lib
./b2 stage
# libraries are now in boost_1_66_0/stage/lib
# include files are in boost_1_66_0/boost
# ( but typically only boost_1_66_0 is added to the include path and you include as
# include <boost/whatever.hpp>