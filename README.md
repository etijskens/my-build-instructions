# my-build-instructions

A collection of installation notes and build scripts for libraries and tools i
often use.

## Boost

Including Boost.Python

## lldbmi2

For using gdb in eclipse on macos

## Vc

Vectorization library for C++. Write explicitly vectorized algorithms without
using intrinsics.
