#!/bin/bash

module purge
module load leibniz/supported
module load intel/2018b
module load CMake

vcroot=/data/antwerpen/201/vsc20170/workspace/Vc
install_prefix=intel/2018b

CXX=icpc CC=icc \
    ${vcroot}/../my-build-instructions/Vc/install-Vc-generic.sh ${vcroot} ${install_prefix}
