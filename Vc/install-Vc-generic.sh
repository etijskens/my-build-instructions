#!/bin/bash
#
if [[ -z "$1" ]]; then
      echo first argument vcroot not set
      exit 1
fi

if [[ -z "2" ]]; then
      echo second argument install_prefix not set
      exit 1
fi
vcroot=$1
install_prefix=$2
cd $vcroot

builddir=$vcroot/build-tmp
mkdir -p $builddir
cd       $builddir

rm -f CMakeCache.txt
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=$vcroot/$install_prefix  \
      -DBUILD_TESTING=OFF \
      $vcroot/Vc
make
make install
