#!/bin/bash

module purge
#module load leibniz/supported
module use /apps/antwerpen/modules/admin-leibniz-centos7
module load leibniz/2019_update1-test
module load intel/2019_update1
module load CMake
module li

vcroot=/data/antwerpen/201/vsc20170/workspace/Vc
install_prefix=intel/2019_update1

CXX=icpc CC=icc \
    ${vcroot}/../my-build-instructions/Vc/install-Vc-generic.sh ${vcroot} ${install_prefix}
